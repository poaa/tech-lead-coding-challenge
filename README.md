# Architecture Overview

## Clean Swift

This project is based on Clean Swift (VIP) approach. 
Each scene (screen) consists from the following components:
- View
- Interactor
- Presenter
- Router
- Models
- Assembly

Such strong decomposition make the solution easily testable, maintainable and extendable.

Clean Swift also follows unidirecctional data flow approach by creating a VIP cycle:
`View -> Interactor -> Presenter -> View`.

We can represent our Clean Swift implementation using UML diagram:
![](Diagrams/SolutionArchitecture/SceneArchitecture.png)


## Modules

Another key concept for guarantee good maintainability is to separate functionality into multiple independent modules + app workspace that use them.
So here we see that we have separate modules for Network, Storage and UI components.

In case if our project will become very big it is possible to convert each screen/scene's VIP representation into separate framework.

P.S. It is needed to add prefix `PD` (Pay Day) into framework's name on iOS to resolve possible name collisions with system frameworks (like with system Network.framework).

<img src="Diagrams/SolutionArchitecture/Components.png" width="600">

## Flows

Each user flow suppose to have its own flow coordinator. It is also described using URL class diagram.
When some flow is finished, next flow coordinator should start. In this project you will see that MainCoordinator is starting its work when AuthCoordinator is finished.

Each screen/scene is created by Assembly which is responsible for scene configuration using DI container or Service Locator like ServiceFactory.
![](Diagrams/SolutionArchitecture/AppArchitecture.png)

## Technologies
- Language: Swift 5
- Architecture: Clean Swift
- Foundation, UIKit
- Deployment target: iOS 12
- Package Management: Cocoapods
- Linter: SwiftLint

In this task I didn't use 3rd party libraries, but we could add in the future:
- `Firebase` (for analytics and remote config)
- `Crashlytics` (for crash reporting)

Native CoreData framework can be used for database management.

# User Stories

There are 4 user stories in the requirements:
- Sign In (implemented)
- Sign Up (not implemented)
- Transactions List (implemented)
- Monthly Dashboard (implemented)

Below you could see the basic flow diagram for all implemented stories.

### Sign In
<img src="Diagrams/Flow/SignIn.png" width="600">

### Transactions List
<img src="Diagrams/Flow/Transactions.png" width="600">

### Monthly Dashboard
<img src="Diagrams/Flow/Dashboard.png" width="600">


## Technical Questions

> 1. **How long did you spend on the coding test? 
> What would you add to your solution if you had more time? 
> If you didn't spend much time on the coding test then use this as an opportunity to explain what you would add.**

I spend 2 full days. The main goad of this task was to provide the architecture concept for MVP app, so I implemented only 3/4 user stories according to priorities. 
In general, most of functionality were implemented except Sign Up screen, but there are some improvements that need more time.

#### Project improvements:
- Add some template for screen's module skeleton generation (Xcode template as an option, http://jeanetienne.net/2017/09/10/advanced-xcode-template.html)
- Add named colors
- Add localization
- Use some code generation tool for localizable text, colors, etc. I prefer `SwiftGen`.
- Add generic mechanism for error handling - `error.localizedDescription` is not good for production usage.
- Cover business logic by unit tests

#### Network improvements:
- Current network layer's mobile implementation doesn't support full access/refresh token management. It is required more time to implement it.
In general we should do several steps to add this functionality into current project:
1. Add field like `isProtected: Bool` into `NetworkRequest` protocol
2. Pass current access token into NetworkClientConfiguration
3. Add `Authorization` header for protected requests inside URLRequestFactory.
4. Add request retrying mechanism for managing token refreshing. Alamofire's approach looks good for me.

#### UI/UX improvements + new features:
- Add screen for reset password
- Adopt interface for multiple accounts and allow to select one of them. 
For now I use only the first `account_id` for fetching transactions to simplify the logic and save some time.
- Adopt UI for iPad. For this we need to add support for iPad's size classes. We could use `UISplitViewController` on iPad and continue using `UITabBarController` on iPhone.
- Redesign the dashboard. For instance we could add some `circle chart (pie chart)` on dashboard screen and assign a unique color for each transaction category.
- Bank support page
- Money Transfer pages
- Biometric Authentication using `TouchID / FaceID`
- Terms / Privacy Policy pages
- Deep Linking / Universal Links
- Remote configuration for further use in A/B testing


> 2. **What was the most useful feature that was added to the latest version of your chosen language? Please include a snippet of code that shows how you've used it.**

I think that collection diffing is very useful feature of Swift 5.1 (https://github.com/apple/swift-evolution/blob/master/proposals/0240-ordered-collection-diffing.md)
We could easily apply this diff inside batch updates to UITableView or UICollectionView.

I used the following code inside `TransactionsViewController.swift`
```Swift
let diff = newData.difference(from: tableItems) { $0.id == $1.id }
// ...
tableView.apply(diff: diff)
```

Also you will find the following extension inside our `PDUIKit` framework:
```swift
extension UITableView {
    
    @available(iOS 13, *)
    public func apply<T>(diff: CollectionDifference<T>, completion: ((Bool) -> Void)? = nil) {
        guard !diff.isEmpty else {
            completion?(true)
            return
        }
        var deletedIndexPaths = [IndexPath]()
        var insertedIndexPaths = [IndexPath]()
        
        for change in diff {
            switch change {
            case let .remove(offset, _, _):
                deletedIndexPaths.append(IndexPath(row: offset, section: 0))
            case let .insert(offset, _, _):
                insertedIndexPaths.append(IndexPath(row: offset, section: 0))
            }
        }
        performBatchUpdates({
            deleteRows(at: deletedIndexPaths, with: .fade)
            insertRows(at: insertedIndexPaths, with: .fade)
        }, completion: completion)
    }
}
```



> 3. **How would you track down a performance issue in production? Have you ever had to do this?**

Sure, I did. In general we should use Xcode Instruments tool for debugging performance issues in most cases. 
- `Time Profiler` will help to find performance bottleneck - it mearures the running time for each method in stack trace in realtime.
- There are also some native instrument that allows to track down `Core Animation` performance issues like off-screen rendering, etc.
- Some issues could be related to memory management, so here we could use `Leaks` and `Allocations`. `Memory Graph` could be used for resolving issues related to retain cycles.
- `Zombie` instrument could help to fix some hard reproducible crashes like `EXC_BAD_ACCESS` in some cases.



> 4. **How would you debug issues related to API usage? Can you give us an example?**
- The simplest way is to use breakpoints and look at the log output, so we have to log every API request / response into console in `debug` mode.
- The second approach / option is to add some debug menu which will be open by device shaking gesture.
Here we will have basic UI for developer and QA with all logged request / response pair.
- Another one option is to use some HTTP proxy like `Charles` (http://charlesproxy.com).
- We will use native `Network Link Conditioner` to test the app behavior on slow internet connection (https://nshipster.com/network-link-conditioner/).
- We can use `Swagger` for network layer's code generation, which allow to keep consistency between latest API version and mobile app and reduce the number of API related issues.
- Cover network layer functionality by unit tests.


> 5. **How would you improve the Node server’s API that you just used?**

There are following things that need to be improved:
- Use HTTS instead of HTTP
- Don't store raw passwords in database. It is better to calculate hash + apply salt to keep our database more secure. 
We also shouldn't send raw password from mobile client to server - intead it is better to send hashed string.
- Authentication API shouldn't send `password` back to the user
- Authentication API should send a pair of "access_token" + "refresh_token" in response for further usage from mobile client side.
- Define a common contract for error handling
- Transactions API should have pagination.
- Resolve inconsistencies for date formats. Currently we have different date formats for customers and accounts - better to choose some unified date format. Transactions `date` field could be represented as `timestamp`.
- Authentication API could return accounts list in response as well:
```
{
  "first_name": "Anton",
  "last_name": "P",
  "gender": "male",
  "email": "anton.test1@example.com",
  "dob": "1993-02-12T08:22:24.377Z",
  "phone": "1049520521",
  "id": 8,
  "accounts": [
      ...
  ]
}
```
- Better to declare JSON keys either in `camelCase` or `snake_case`. It is not a common practice to use whitespaces in the middle ("First Name").
```
{
  "first_name": "Anton",
  "last_name": "P",
  // ...
}
```


> **Please describe yourself using JSON**

```
{
    "fullName": "Anton Poltoratskyi",
    "company": "Andersen",
    "position": "Tech Lead / Senior iOS developer",
    "hardSkills": {
        "common": [
            "Software Design",
            "Algorithms",
            "Development Metodologies",
            "Estimations"
        ],
        "platforms": [
            "iOS" 
        ],
        "languages": [
            "Swift", "Objective-C", "C"
        ],
        "architecture": [
            "MVC", "MVP", "VIP (Clean Swift)", "VIPER"
        ],
        "nativeSDK": [
            "Foundation", "UIKit", "CoreData", "SQLite", "GCD", "Operations", "URLSession", "UserNotifications", "MapKit", "SpriteKit"
        ],
        "thirdPartySDK": [
            "SnapKit", "Alamofire", "GRDB", "SDWebImage", "KingFisher", "GoogleMaps"
        ],
        "testing": [
            "XCTest"
        ],
        "VCS": [
            "Git"
        ],
        "dependencyManagers": [
            "CocoaPods",
            "Carthage"
        ]
    },
    "softSkills": [
        "Leadership", "Communication", "Research", "Team Player"
    ],
    "spokenLanguages": [
        {
            "language" : "Ukrainian",
            "level" : "Native"
        },
        {
            "language" : "Russian",
            "level" : "Native"
        },
        {
            "language" : "English",
            "level" : "B1"
        }
    ]
}
```
