//
//  AlertPresentable.swift
//  PDUIKit
//
//  Created by Anton Poltoratskyi on 04.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import UIKit

public protocol AlertPresentable: AnyObject {
    func showAlert(_ title: String?, message: String?, actionHandler: (() -> Void)?, completion: (() -> Void)?)
}

public protocol AlertDisplayable: AlertPresentable {
    var viewController: UIViewController? { get }
}

extension AlertDisplayable {
    
    public func showAlert(_ title: String?, message: String?, actionHandler: (() -> Void)?, completion: (() -> Void)?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .default) { _ in
            actionHandler?()
        }
        alert.addAction(alertAction)
        viewController?.present(alert, animated: true, completion: completion)
    }
}
