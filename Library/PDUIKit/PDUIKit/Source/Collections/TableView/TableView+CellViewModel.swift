//
//  TableView+CellViewModel.swift
//  PDUIKit
//
//  Created by Anton Poltoratskyi on 05.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import UIKit

extension UITableView {
    
    public func dequeueReusableCell(with viewModel: AnyCellViewModel, for indexPath: IndexPath) -> UITableViewCell {
        let identifier = type(of: viewModel).uniqueIdentifier
        let cell = dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        viewModel.setup(cell: cell)
        return cell
    }
    
    public func register(_ modelType: AnyCellViewModel.Type) {
        if let nibFileName = (modelType.cellClass as? NibInitializable.Type)?.nibFileName {
            let nib = UINib(nibName: nibFileName, bundle: Bundle(for: modelType.cellClass))
            register(nib, forCellReuseIdentifier: modelType.uniqueIdentifier)
            
        } else {
            register(modelType.cellClass, forCellReuseIdentifier: modelType.uniqueIdentifier)
        }
    }
    
    public func register(_ models: [AnyCellViewModel.Type]) {
        models.forEach { register($0) }
    }
    
    public func register(_ models: AnyCellViewModel.Type...) {
        models.forEach { register($0) }
    }
    
    public func register<T: CellViewModel>(_ viewModel: T.Type) where T.Cell: UITableViewCell {
        register(T.Cell.self, forCellReuseIdentifier: T.uniqueIdentifier)
    }
    
    public func register<T: CellViewModel>(_ viewModel: T.Type) where T.Cell: UITableViewCell, T.Cell: NibInitializable {
        let nib = UINib(nibName: T.Cell.nibFileName, bundle: Bundle(for: T.Cell.self))
        register(nib, forCellReuseIdentifier: T.uniqueIdentifier)
    }
}
