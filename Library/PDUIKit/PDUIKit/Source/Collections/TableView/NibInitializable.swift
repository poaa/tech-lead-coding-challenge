//
//  NibInitializable.swift
//  PDUIKit
//
//  Created by Anton Poltoratskyi on 05.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import UIKit

public protocol NibInitializable: AnyObject {
    static var nibFileName: String { get }
}

extension NibInitializable where Self: UIView {

    public static var nibFileName: String {
        return String(describing: self)
    }
}

extension NibInitializable where Self: UIViewController {
    
    public static var nibFileName: String {
        return String(describing: self)
    }
    
    public static func instantiateFromNib() -> Self {
        return Self.init(nibName: nibFileName, bundle: Bundle(for: Self.self))
    }
}
