//
//  CellViewModel.swift
//  PDUIKit
//
//  Created by Anton Poltoratskyi on 05.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import UIKit

public typealias AnyViewCell = UIView

public protocol AnyCellViewModel {
    static var cellClass: AnyClass { get }
    static var uniqueIdentifier: String { get }
    func setup(cell: AnyViewCell)
    func height(constrainedBy maxWidth: CGFloat) -> CGFloat?
}

extension AnyCellViewModel {
    public static var uniqueIdentifier: String {
        return String(describing: self)
    }
    
    public func height(constrainedBy maxWidth: CGFloat) -> CGFloat? {
        return nil
    }
}

public protocol CellViewModel: AnyCellViewModel {
    associatedtype Cell: AnyViewCell
    func setup(cell: Cell)
}

extension CellViewModel {
    public static var cellClass: AnyClass {
        return Cell.self
    }
    
    public func setup(cell: AnyViewCell) {
        // swiftlint:disable force_cast
        setup(cell: cell as! Cell)
        // swiftlint:enable force_cast
    }
}
