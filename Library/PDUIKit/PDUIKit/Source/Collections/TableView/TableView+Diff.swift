//
//  TableView+Diff.swift
//  PDUIKit
//
//  Created by Anton Poltoratskyi on 06.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import UIKit

extension UITableView {
    
    @available(iOS 13, *)
    public func apply<T>(diff: CollectionDifference<T>, completion: ((Bool) -> Void)? = nil) {
        guard !diff.isEmpty else {
            completion?(true)
            return
        }
        var deletedIndexPaths = [IndexPath]()
        var insertedIndexPaths = [IndexPath]()
        
        for change in diff {
            switch change {
            case let .remove(offset, _, _):
                deletedIndexPaths.append(IndexPath(row: offset, section: 0))
            case let .insert(offset, _, _):
                insertedIndexPaths.append(IndexPath(row: offset, section: 0))
            }
        }
        performBatchUpdates({
            deleteRows(at: deletedIndexPaths, with: .fade)
            insertRows(at: insertedIndexPaths, with: .fade)
        }, completion: completion)
    }
}
