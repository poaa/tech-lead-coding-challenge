//
//  KeyboardInputView.swift
//  PDUIKit
//
//  Created by Anton Poltoratskyi on 04.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import UIKit

public typealias KeyboardInputView = UIView & InputView

public protocol InputView: AnyObject {
    var isActive: Bool { get }
    func beginEditing()
}

extension UITextField: InputView {
    
    public func beginEditing() {
        becomeFirstResponder()
    }
    
    public var isActive: Bool {
        return isFirstResponder
    }
}
