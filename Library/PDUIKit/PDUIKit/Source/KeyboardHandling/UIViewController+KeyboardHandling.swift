//
//  UIViewController+KeyboardHandling.swift
//  PDUIKit
//
//  Created by Anton Poltoratskyi on 04.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import UIKit

extension UIViewController {
    
    public func registerForKeyboardNotifications() {
        let center = NotificationCenter.default
        center.addObserver(self,
                           selector: #selector(handleKeyboardWillShow(notification:)),
                           name: UIResponder.keyboardWillShowNotification,
                           object: nil)
        
        center.addObserver(self,
                           selector: #selector(handleKeyboardWillHide(notification:)),
                           name: UIResponder.keyboardWillHideNotification,
                           object: nil)
    }
    
    public func unregisterForKeyboardNotifications() {
        let center = NotificationCenter.default
        center.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        center.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func handleKeyboardWillShow(notification: Notification) {
        guard let userInfo = notification.userInfo, let keyboard = KeyboardInfo(userInfo: userInfo) else {
            return
        }
        (self as? KeyboardHandler)?.handleKeyboardShow(keyboard)
    }
    
    @objc private func handleKeyboardWillHide(notification: Notification) {
        guard let userInfo = notification.userInfo, let keyboard = KeyboardInfo(userInfo: userInfo) else {
            return
        }
        (self as? KeyboardHandler)?.handleKeyboardHide(keyboard)
    }
}
