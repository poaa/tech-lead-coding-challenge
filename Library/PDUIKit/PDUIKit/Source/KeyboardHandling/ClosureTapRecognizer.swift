//
//  ClosureTapRecognizer.swift
//  PDUIKit
//
//  Created by Anton Poltoratskyi on 04.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import UIKit

final class ClosureTapRecognizer: UITapGestureRecognizer {
    
    let identifier: String
    let action: ((UITapGestureRecognizer) -> Void)?
    
    init(identifier: String, action: ((UITapGestureRecognizer) -> Void)? = nil) {
        self.identifier = identifier
        self.action = action
        super.init(target: nil, action: nil)
        addTarget(self, action: #selector(tapDidRecognized(recognizer:)))
    }
    
    @objc private func tapDidRecognized(recognizer: UITapGestureRecognizer) {
        action?(recognizer)
    }
}
