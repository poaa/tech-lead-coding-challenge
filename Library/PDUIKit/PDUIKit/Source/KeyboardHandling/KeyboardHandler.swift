//
//  KeyboardInteractive.swift
//  PDUIKit
//
//  Created by Anton Poltoratskyi on 04.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import UIKit

public protocol KeyboardHandler where Self: UIViewController {
    var keyboardScrollView: UIScrollView { get }
    var keyboardInputViews: [KeyboardInputView] { get }
    var activeView: KeyboardInputView? { get }
    
    var shouldHideKeyboardOnTap: Bool { get }

    func moveToNextInput()
    
    func handleKeyboardShow(_ keyboard: KeyboardInfo)
    func handleKeyboardHide(_ keyboard: KeyboardInfo)
}

// MARK: - Keyboard Notification

extension KeyboardHandler {
    
    public func handleKeyboardShow(_ keyboard: KeyboardInfo) {
        guard let activeInputView = activeView else {
            return
        }
        if shouldHideKeyboardOnTap {
            addTapGesture()
        }
        let keyboardHeight = height(of: keyboard)
        
        setScrollInset(keyboardHeight)
        
        // If active text field is hidden by keyboard, scroll it so it's visible.
        let activeViewFrame = activeInputView.frame
        guard let activeRect = activeInputView.superview?.convert(activeViewFrame, to: keyboardScrollView) else {
            return
        }
        var rootFrame = view.frame
        rootFrame.size.height -= keyboardHeight
        
        let animations: () -> Void
        
        if !rootFrame.contains(activeRect) {
            animations = {
                self.keyboardScrollView.scrollRectToVisible(activeRect, animated: false)
                self.fixImplicitInputFieldsAnimation()
                self.view.layoutIfNeeded()
            }
        } else {
            animations = {
                self.fixImplicitInputFieldsAnimation()
                self.view.layoutIfNeeded()
            }
        }
        animate(with: keyboard, animations: animations)
    }
    
    public func handleKeyboardHide(_ keyboard: KeyboardInfo) {
        removeTapGesture()
        restoreScrollInset()
        animate(with: keyboard) {
            self.fixImplicitInputFieldsAnimation()
            self.view.layoutIfNeeded()
        }
    }
    
    /// When you set custom inset for textRect by overriding `func textRect(forBounds:)` (see BrandTextField) - you would
    /// have unexpected implicit animations in UITextField.
    /// Please check https://stackoverflow.com/questions/50757053/uitextfield-bouncing-text for details.
    private func fixImplicitInputFieldsAnimation() {
        UIView.performWithoutAnimation {
            keyboardInputViews.forEach { $0.layoutIfNeeded() }
        }
    }
    
    private func height(of keyboard: KeyboardInfo) -> CGFloat {
        var rootViewBottomOffset: CGFloat = 0
        let bottomPoint = CGPoint(x: view.frame.midX, y: view.frame.maxY)
        
        if let window = view.window, let bottomInWindow = view.superview?.convert(bottomPoint, to: view.window).y {
            rootViewBottomOffset = window.bounds.height - bottomInWindow
        }
        
        return keyboard.height - rootViewBottomOffset
    }
    
    private func animate(with keyboard: KeyboardInfo, animations: @escaping () -> Void) {
        let options = UIView.AnimationOptions(rawValue: UInt(keyboard.curve.rawValue))
        UIView.animate(withDuration: keyboard.duration, delay: 0, options: options, animations: {
            animations()
        }, completion: nil)
    }
    
    private func setScrollInset(_ bottomInset: CGFloat) {
        var scrollViewBottomOffset: CGFloat = 0
        let bottomPoint = CGPoint(x: keyboardScrollView.frame.midX, y: keyboardScrollView.frame.maxY)
        
        if let window = view.window, let bottomInWindow = keyboardScrollView.superview?.convert(bottomPoint, to: view.window).y {
            scrollViewBottomOffset = window.bounds.height - bottomInWindow
        }
        
        let bottomInset = bottomInset - scrollViewBottomOffset
        
        keyboardScrollView.contentInset.bottom = bottomInset - keyboardScrollView.safeAreaInsets.bottom
        keyboardScrollView.scrollIndicatorInsets.bottom = bottomInset - keyboardScrollView.safeAreaInsets.bottom
    }
    
    private func restoreScrollInset() {
        keyboardScrollView.contentInset.bottom = keyboardScrollView.safeAreaInsets.bottom
        keyboardScrollView.scrollIndicatorInsets.bottom = keyboardScrollView.safeAreaInsets.bottom
    }
}

// MARK: - Input Views

extension KeyboardHandler {
    
    public var activeView: KeyboardInputView? {
        return keyboardInputViews.first { $0.isActive }
    }
    
    public func moveToNextInput() {
        guard let activeView = activeView, let activeIndex = keyboardInputViews.firstIndex(where: { $0 === activeView }) else {
            return
        }
        if keyboardInputViews.indices.contains(activeIndex + 1) {
            keyboardInputViews[activeIndex + 1].beginEditing()
        } else {
            activeView.endEditing(true)
        }
    }
}

// MARK: - Tap Gesture

extension KeyboardHandler {
    
    public var shouldHideKeyboardOnTap: Bool {
        return true
    }
    
    private var tapIdentifier: String {
        return "tap-on-whitespace"
    }
    
    private func addTapGesture() {
        /// Workaround to use gesture recognizer in protocol extension with Obj-C constraint (UIViewController).
        /// Swift 4+ requires that methods called with selectors must be marked as @objc which is not allowed in protocol extentions.
        let tapRecognizer = ClosureTapRecognizer(identifier: tapIdentifier) { [weak self] recognizer in
            self?.tapOnWhiteSpace(recognizer: recognizer)
        }
        
        if view.gestureRecognizers == nil || view.gestureRecognizers?.first(where: { ($0 as? ClosureTapRecognizer)?.identifier == tapIdentifier }) == nil {
            view.addGestureRecognizer(tapRecognizer)
        }
    }
    
    private func removeTapGesture() {
        let identifier = tapIdentifier
        if let recognizer = view.gestureRecognizers?.first(where: { ($0 as? ClosureTapRecognizer)?.identifier == identifier }) {
            view.removeGestureRecognizer(recognizer)
        }
    }
    
    private func tapOnWhiteSpace(recognizer: UITapGestureRecognizer) {
        activeView?.endEditing(true)
    }
}
