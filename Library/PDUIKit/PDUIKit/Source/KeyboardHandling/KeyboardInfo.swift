//
//  KeyboardInfo.swift
//  PDUIKit
//
//  Created by Anton Poltoratskyi on 04.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import UIKit

public struct KeyboardInfo {
    public let height: CGFloat
    public let duration: Double
    public let curve: UIView.AnimationCurve
    
    public init?(userInfo: [AnyHashable: Any]) {
        guard let height = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height,
            let duration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue,
            let rawCurve = (userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber)?.intValue,
            let curve = UIView.AnimationCurve(rawValue: rawCurve) else {
                return nil
        }
        self.height = height
        self.duration = duration
        self.curve = curve
    }
}
