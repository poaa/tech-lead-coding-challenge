//
//  BrandButton.swift
//  PDUIKit
//
//  Created by Anton Poltoratskyi on 04.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import UIKit

open class BrandButton: UIButton {
    
    open var cornerRadius: CGFloat = 8 {
        didSet {
            setupCorners()
        }
    }
    
    open override var isEnabled: Bool {
        didSet {
            setupState()
        }
    }
    
    open override var intrinsicContentSize: CGSize {
        var size = super.intrinsicContentSize
        size.height = 44
        return size
    }
    
    // MARK: - Init
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: - Setup
    
    private func setup() {
        setContentHuggingPriority(.required, for: .vertical)
        setContentCompressionResistancePriority(.required, for: .vertical)
        setTitleColor(UIColor.white, for: .normal)
        setupState()
    }
    
    private func setupCorners() {
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = true
    }
    
    private func setupState() {
        alpha = isEnabled ? 1 : 0.4
    }
    
    // MARK: - Life Cycle
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        setupCorners()
    }
}
