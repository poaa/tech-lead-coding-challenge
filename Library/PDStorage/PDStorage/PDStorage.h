//
//  PDStorage.h
//  PDStorage
//
//  Created by Anton Poltoratskyi on 04.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for PDStorage.
FOUNDATION_EXPORT double PDStorageVersionNumber;

//! Project version string for PDStorage.
FOUNDATION_EXPORT const unsigned char PDStorageVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <PDStorage/PublicHeader.h>


