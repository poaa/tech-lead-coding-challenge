//
//  KeychainStorage.swift
//  PDStorage
//
//  Created by Anton Poltoratskyi on 04.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

public final class KeychainStorage {
    
    private let serviceName: String
    
    private let accessGroup: String?
    
    public init(serviceName: String, accessGroup: String? = nil) {
        self.serviceName = serviceName
        self.accessGroup = accessGroup
    }
    
    public func value(forKey key: String) throws -> String {
        let item = passwordItem(forKey: key)
        return try item.readPassword()
    }
    
    public func setValue(_ value: String, forKey key: String) throws {
        let item = passwordItem(forKey: key)
        try item.savePassword(value)
    }
    
    public func setValue(_ value: Int, forKey key: String) throws {
        let item = passwordItem(forKey: key)
        try item.savePassword(String(value))
    }
    
    public func removeValue(_ value: String, forKey key: String) throws {
        let item = passwordItem(forKey: key)
        try item.deleteItem()
    }
    
    private func passwordItem(forKey key: String) -> KeychainPasswordItem {
        return KeychainPasswordItem(
            service: serviceName,
            account: key,
            accessGroup: accessGroup
        )
    }
}
