Pod::Spec.new do |spec|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  spec.name         = "PDStorage"
  spec.version      = "1.0.0"
  spec.summary      = "A short description of PDStorage."
  spec.description  = <<-DESC
                  Library with shared components for storing data
                  DESC

  spec.homepage     = "http://EXAMPLE/PDStorage"

  # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  spec.license      = "MIT"
  spec.author             = { "Anton Poltoratskyi" => "a.poltoratskyi@andersenlab.com" }

  # ――― Platform Specifics ――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  spec.platform     = :ios, "12.4"

  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  spec.source       = { :git => "https://github.com/AntonPoltoratskyi/TechLeadCodingChallenge.git", :tag => "#{spec.version}" }

  # ――― Source Code ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  spec.source_files  = "PDStorage/Source/**/*.{swift}"
  
end
