//
//  TransactionsResponse.swift
//  PDNetwork
//
//  Created by Anton Poltoratskyi on 05.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

public enum TransactionsResponse {
    
    public struct Transaction: Decodable {
        public let id: Int
        public let accountId: Int
        public let amount: String
        public let vendor: String
        public let category: String
        public let date: String
        
        enum CodingKeys: String, CodingKey {
            case id
            case accountId = "account_id"
            case amount
            case vendor
            case category
            case date
        }
    }
}
