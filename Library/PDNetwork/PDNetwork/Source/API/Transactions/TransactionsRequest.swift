//
//  TransactionsRequest.swift
//  PDNetwork
//
//  Created by Anton Poltoratskyi on 05.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

public struct TransactionsRequest: NetworkRequest {
    public let path: String
    public let queryItems: HTTPQueryItems?
    public let method: HTTPMethod = .get
    
    public init(accountId: Int) {
        path = "/transactions"
        queryItems = [
            "account_id": String(accountId)
        ]
    }
}
