//
//  SignInRequest.swift
//  PDNetwork
//
//  Created by Anton Poltoratskyi on 04.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

public struct SignInRequest: NetworkRequest {
    public let path: String
    public let params: HTTPParameters?
    public let method: HTTPMethod = .post
    
    public init(email: String, password: String) {
        path = "/authenticate"
        params = [
            "email": email,
            "password": password
        ]
    }
}
