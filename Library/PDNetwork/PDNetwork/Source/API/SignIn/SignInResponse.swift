//
//  SignInResponse.swift
//  PDNetwork
//
//  Created by Anton Poltoratskyi on 04.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

public struct SignInResponse: Decodable {
    public let id: Int
    public let firstName: String
    public let lastName: String
    public let gender: Gender
    public let email: String
    public let password: String
    public let dateOfBirth: String
    public let phone: String
    
    public enum Gender: String, Decodable {
        case male
        case female
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case firstName = "First Name"
        case lastName = "Last Name"
        case gender
        case email
        case password
        case dateOfBirth = "dob"
        case phone
    }
}
