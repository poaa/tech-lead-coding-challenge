//
//  AccountRequest.swift
//  PDNetwork
//
//  Created by Anton Poltoratskyi on 05.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

public struct AccountRequest: NetworkRequest {
    public let path: String
    public let queryItems: HTTPQueryItems?
    public let method: HTTPMethod = .get
    
    public init(customerId: Int) {
        path = "/accounts"
        queryItems = [
            "customer_id": String(customerId)
        ]
    }
}
