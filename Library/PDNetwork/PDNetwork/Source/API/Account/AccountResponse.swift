//
//  AccountResponse.swift
//  PDNetwork
//
//  Created by Anton Poltoratskyi on 05.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

public enum AccountResponse {
    
    public struct Account: Decodable {
        public let id: Int
        public let customerId: Int
        public let active: Bool
        public let type: String
        public let iban: String
        public let createdDate: String
        
        enum CodingKeys: String, CodingKey {
            case id
            case customerId = "customer_id"
            case active
            case type
            case iban
            case createdDate = "date_created"
        }
    }
}
