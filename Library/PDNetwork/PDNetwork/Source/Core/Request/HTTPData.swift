//
//  HTTPData.swift
//  PDNetwork
//
//  Created by Anton Poltoratskyi on 04.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

public typealias HTTPHeaders = [String: String]

public typealias HTTPParameters = [String: Any]

public typealias HTTPQueryItems = [String: String]
