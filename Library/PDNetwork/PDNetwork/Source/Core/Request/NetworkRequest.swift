//
//  NetworkRequest.swift
//  PDNetwork
//
//  Created by Anton Poltoratskyi on 04.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import Foundation

public enum NetworkRequestError: Error {
    case invalidURL
}

public protocol NetworkRequest {
    var baseURL: URL? { get }
    var path: String { get }
    var method: HTTPMethod { get }
    var params: HTTPParameters? { get }
    var headers: HTTPHeaders { get }
    var queryItems: HTTPQueryItems? { get }
}

extension NetworkRequest {
    
    public var baseURL: URL? {
        return nil
    }
    
    public var queryItems: HTTPQueryItems? {
        return [:]
    }
    
    public var params: HTTPParameters? {
        return nil
    }
    
    public var headers: HTTPHeaders {
        return [:]
    }
}
