//
//  URLRequestFactory.swift
//  PDNetwork
//
//  Created by Anton Poltoratskyi on 04.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import Foundation

protocol URLRequestFactory {
    func makeURLRequest(for request: NetworkRequest, configuration: NetworkClientConfiguration) throws -> URLRequest
}

final class URLRequestFactoryImpl: URLRequestFactory {
    
    func makeURLRequest(for request: NetworkRequest, configuration: NetworkClientConfiguration) throws -> URLRequest {
        let url = try makeURL(for: request, configuration: configuration)
        
        var urlRequest = URLRequest(url: url)
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.httpBody = try request.params.map { try JSONSerialization.data(withJSONObject: $0) }
        urlRequest.httpMethod = request.method.rawValue
        
        for (key, value) in request.headers {
            urlRequest.setValue(value, forHTTPHeaderField: key)
        }
        
        return urlRequest
    }
    
    private func makeURL(for request: NetworkRequest, configuration: NetworkClientConfiguration) throws -> URL {
        let baseURL = (request.baseURL ?? configuration.baseURL).appendingPathComponent(request.path)
        
        var urlComponents = URLComponents(url: baseURL, resolvingAgainstBaseURL: false)
        if let queryItems = request.queryItems, !queryItems.isEmpty {
            urlComponents?.queryItems = queryItems.map { URLQueryItem(name: $0, value: $1) }
        }
        guard let url = urlComponents?.url else {
            throw NetworkRequestError.invalidURL
        }
        return url
    }
}
