//
//  URLSessionNetworkClient.swift
//  PDNetwork
//
//  Created by Anton Poltoratskyi on 04.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import Foundation

final class URLSessionNetworkClient: NetworkClient {
    
    var configuration: NetworkClientConfiguration
    
    private let session: URLSessionProtocol
    
    private let requestFactory: URLRequestFactory
    
    init(configuration: NetworkClientConfiguration,
         session: URLSessionProtocol,
         requestFactory: URLRequestFactory) {
        self.configuration = configuration
        self.session = session
        self.requestFactory = requestFactory
    }
    
    func perform<Response: Decodable>(_ request: NetworkRequest,
                                      notifyOn queue: DispatchQueue,
                                      decoder: JSONDecoder,
                                      completion: ((Result<Response, Error>) -> Void)?) {
        guard let urlRequest = try? requestFactory.makeURLRequest(for: request, configuration: configuration) else {
            completion?(.failure(NetworkError.invalidURLRequest))
            return
        }
        let task = session.dataTask(with: urlRequest) { data, urlResponse, error in
            func handle(_ result: Result<Response, Error>) {
                queue.async {
                    completion?(result)
                }
            }
            
            if let error = error {
                handle(.failure(error))
            } else if let data = data {
                do {
                    let decodedData = try decoder.decode(Response.self, from: data)
                    handle(.success(decodedData))
                } catch {
                    handle(.failure(NetworkError.jsonDecodingError(error)))
                }
            }
        }
        task.resume()
    }
}
