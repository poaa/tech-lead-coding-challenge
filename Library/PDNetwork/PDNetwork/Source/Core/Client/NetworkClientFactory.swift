//
//  NetworkClientFactory.swift
//  PDNetwork
//
//  Created by Anton Poltoratskyi on 04.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import Foundation

public protocol NetworkClientFactory {
    func makeNetworkClient(configuration: NetworkClientConfiguration) -> NetworkClient
}

public final class URLSessionNetworkClientFactory: NetworkClientFactory {
    
    public init() {
    }
    
    public func makeNetworkClient(configuration: NetworkClientConfiguration) -> NetworkClient {
        let session = URLSession.shared
        let requestFactory = URLRequestFactoryImpl()
        
        return URLSessionNetworkClient(
            configuration: configuration,
            session: session,
            requestFactory: requestFactory
        )
    }
}
