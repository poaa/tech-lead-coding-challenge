//
//  NetworkClient.swift
//  PDNetwork
//
//  Created by Anton Poltoratskyi on 04.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import Foundation

public protocol NetworkClient: AnyObject {
    
    var configuration: NetworkClientConfiguration { get set }
    
    func perform<Response: Decodable>(_ request: NetworkRequest,
                                      completion: ((Result<Response, Error>) -> Void)?)
    
    func perform<Response: Decodable>(_ request: NetworkRequest,
                                      notifyOn queue: DispatchQueue,
                                      completion: ((Result<Response, Error>) -> Void)?)
    
    func perform<Response: Decodable>(_ request: NetworkRequest,
                                      notifyOn queue: DispatchQueue,
                                      decoder: JSONDecoder,
                                      completion: ((Result<Response, Error>) -> Void)?)
}

extension NetworkClient {
    
    public func perform<Response: Decodable>(_ request: NetworkRequest,
                                      completion: ((Result<Response, Error>) -> Void)?) {
        perform(request, notifyOn: .main, completion: completion)
    }
    
    public func perform<Response: Decodable>(_ request: NetworkRequest,
                                             notifyOn queue: DispatchQueue,
                                             completion: ((Result<Response, Error>) -> Void)?) {
        let defaultDecoder = JSONDecoder()
        perform(request, notifyOn: queue, decoder: defaultDecoder, completion: completion)
    }
}
