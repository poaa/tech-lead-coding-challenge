Pod::Spec.new do |spec|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  spec.name         = "PDNetwork"
  spec.version      = "1.0.0"
  spec.summary      = "A short description of PDNetwork."
  spec.description  = <<-DESC
                  Network library
                  DESC

  spec.homepage     = "http://EXAMPLE/PDNetwork"

  # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  spec.license      = "MIT"
  spec.author             = { "Anton Poltoratskyi" => "a.poltoratskyi@andersenlab.com" }

  # ――― Platform Specifics ――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  spec.platform     = :ios, "12.4"

  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  spec.source       = { :git => "https://github.com/AntonPoltoratskyi/TechLeadCodingChallenge.git", :tag => "#{spec.version}" }

  # ――― Source Code ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #

  spec.source_files  = "PDNetwork/Source/**/*.{swift}"
  
end
