//
//  TransactionsPresenter.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 05.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import Foundation

final class TransactionsPresenter: TransactionsInteractorOutput {
    
    private weak var view: TransactionsViewInput?
    
    private let dateFormatter = TransactionDateFormatter()
    
    private let decimalFormatter = DecimalFormatter()
    
    // MARK: - Init
    
    init(view: TransactionsViewInput) {
        self.view = view
    }
    
    // MARK: - Presentation
    
    func presentScreen(response: TransactionsModels.Screen.Response) {
        let viewModel = TransactionsModels.Screen.ViewModel(
            title: "My Transactions"
        )
        view?.displayScreen(viewModel: viewModel)
    }
    
    func presentTransactions(response: TransactionsModels.Load.Response) {
        let viewModel = TransactionsModels.Load.ViewModel(transactions: response.transactions.map {
            TransactionViewModel(
                id: $0.id,
                amount: decimalFormatter.string(from: $0.amount),
                vendor: $0.vendor,
                category: $0.category,
                date: dateFormatter.string(from: $0.date)
            )
        })
        view?.displayTransactions(viewModel: viewModel)
    }
    
    func presentFailure(response: FailureResponse) {
        let viewModel = FailureViewModel(
            title: "Error",
            errorMessage: response.errorMessage
        )
        view?.displayFailure(viewModel: viewModel)
    }
    
    func presentLoading(isLoading: Bool) {
        view?.displayLoading(isLoading: isLoading)
    }
}
