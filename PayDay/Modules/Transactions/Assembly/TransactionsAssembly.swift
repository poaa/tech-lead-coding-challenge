//
//  TransactionsAssembly.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 05.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import UIKit
import PDUIKit

struct TransactionsAssembly {
    
    private let serviceFactory: ServiceFactory
    
    init(serviceFactory: ServiceFactory) {
        self.serviceFactory = serviceFactory
    }
    
    func build() -> UIViewController {
        let viewController = TransactionsViewController.instantiateFromNib()
        let router = TransactionsRouter(viewController: viewController)
        let presenter = TransactionsPresenter(view: viewController)
        let interactor = TransactionsInteractor(
            presenter: presenter,
            transactionsWorker: serviceFactory.makeTransactionsWorker()
        )
        viewController.set(interactor: interactor, router: router)
        
        return viewController
    }
}
