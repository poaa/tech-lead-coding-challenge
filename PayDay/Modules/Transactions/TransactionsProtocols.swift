//
//  TransactionsProtocols.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 05.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import PDUIKit

// MARK: - View

protocol TransactionsViewInput: AnyObject, LoadingDisplayable, FailureDisplayable {
    func displayScreen(viewModel: TransactionsModels.Screen.ViewModel)
    func displayTransactions(viewModel: TransactionsModels.Load.ViewModel)
}

// MARK: - Interactor

protocol TransactionsInteractorInput {
    func loadScreen(request: TransactionsModels.Screen.Request)
    func loadTransactions(request: TransactionsModels.Load.Request)
}

// MARK: - Presenter

protocol TransactionsInteractorOutput: LoadingPresentable, FailurePresentable {
    func presentScreen(response: TransactionsModels.Screen.Response)
    func presentTransactions(response: TransactionsModels.Load.Response)
}

// MARK: - Router

protocol TransactionsRouterInput: AlertPresentable {
}
