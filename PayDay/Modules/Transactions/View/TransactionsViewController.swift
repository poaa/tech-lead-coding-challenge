//
//  TransactionsViewController.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 05.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import UIKit
import PDUIKit

final class TransactionsViewController: UIViewController, NibInitializable, TransactionsViewInput {
    
    private var interactor: TransactionsInteractorInput?
    
    private var router: TransactionsRouterInput?
    
    private var tableItems: [TransactionViewModel] = []
    
    // MARK: - Subviews
    
    @IBOutlet private var tableView: UITableView!
    
    @IBOutlet private var activityIndicator: UIActivityIndicatorView!
    
    // MARK: - Injection
    
    func set(interactor: TransactionsInteractorInput, router: TransactionsRouterInput) {
        self.interactor = interactor
        self.router = router
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        loadScreen()
        loadData()
    }
    
    private func setupUI() {
        tableView.tableFooterView = UIView()
        tableView.register(TransactionViewModel.self)
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(actionRefreshEventTriggered(sender:)), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    private func loadScreen() {
        let request = TransactionsModels.Screen.Request()
        interactor?.loadScreen(request: request)
    }
    
    private func loadData() {
        let request = TransactionsModels.Load.Request()
        interactor?.loadTransactions(request: request)
    }
    
    // MARK: - Actions
    
    @objc private func actionRefreshEventTriggered(sender: UIRefreshControl) {
        loadData()
    }
    
    // MARK: - View Input
    
    func displayScreen(viewModel: TransactionsModels.Screen.ViewModel) {
        navigationItem.title = viewModel.title
    }
    
    func displayTransactions(viewModel: TransactionsModels.Load.ViewModel) {
        tableView.refreshControl?.endRefreshing()
        
        let newData = viewModel.transactions
        
        guard !tableItems.isEmpty, #available(iOS 13, *) else {
            tableItems = newData
            tableView.reloadData()
            return
        }
        let diff = newData.difference(from: tableItems) { $0.id == $1.id }
        
        tableItems = newData
        tableView.apply(diff: diff)
    }
    
    func displayFailure(viewModel: FailureViewModel) {
        router?.showAlert(viewModel.title, message: viewModel.errorMessage, actionHandler: nil, completion: nil)
    }
    
    func displayLoading(isLoading: Bool) {
        if isLoading {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
    }
}

// MARK: - Table View

extension TransactionsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(with: tableItems[indexPath.row], for: indexPath)
    }
}

extension TransactionsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
