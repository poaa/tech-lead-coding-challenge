//
//  TransactionTableViewCell.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 05.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import UIKit
import PDUIKit

final class TransactionTableViewCell: UITableViewCell, NibInitializable {

    // MARK: - Subviews
    
    @IBOutlet private var vendorLabel: UILabel!
    
    @IBOutlet private var categoryLabel: UILabel!
    
    @IBOutlet private var amountLabel: UILabel!
    
    @IBOutlet private var dateLabel: UILabel!
    
    // MARK: - Setup
    
    func setup(data: TransactionViewModelData) {
        vendorLabel.text = data.vendor
        categoryLabel.text = data.category
        amountLabel.text = data.amount
        dateLabel.text = data.date
    }
}
