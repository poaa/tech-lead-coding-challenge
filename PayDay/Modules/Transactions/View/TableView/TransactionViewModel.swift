//
//  TransactionViewModel.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 05.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import PDUIKit

protocol TransactionViewModelData {
    var amount: String { get }
    var vendor: String { get }
    var category: String { get }
    var date: String { get }
}

struct TransactionViewModel: CellViewModel, TransactionViewModelData {
    let id: Int
    let amount: String
    let vendor: String
    let category: String
    let date: String
    
    func setup(cell: TransactionTableViewCell) {
        cell.setup(data: self)
    }
}
