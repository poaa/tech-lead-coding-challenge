//
//  TransactionsEntity.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 05.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import Foundation

enum TransactionsModels {
    
    enum Screen {
        struct Request {
        }
        struct Response {
        }
        struct ViewModel {
            let title: String
        }
    }
    
    enum Load {
        struct Request {
        }
        struct Response {
            struct Transaction {
                let id: Int
                let amount: Decimal
                let vendor: String
                let category: String
                let date: Date
            }
            let transactions: [Transaction]
        }
        struct ViewModel {
            let transactions: [TransactionViewModel]
        }
    }
}
