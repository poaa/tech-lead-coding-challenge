//
//  TransactionsInteractor.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 05.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

final class TransactionsInteractor: TransactionsInteractorInput {
    
    private let presenter: TransactionsInteractorOutput
    
    private let transactionsWorker: TransactionsWorker
    
    // MARK: - Init
    
    init(presenter: TransactionsInteractorOutput, transactionsWorker: TransactionsWorker) {
        self.presenter = presenter
        self.transactionsWorker = transactionsWorker
    }
    
    // MARK: - Interactor Input
    
    func loadScreen(request: TransactionsModels.Screen.Request) {
        let response = TransactionsModels.Screen.Response()
        presenter.presentScreen(response: response)
    }
    
    func loadTransactions(request: TransactionsModels.Load.Request) {
        presenter.presentLoading(isLoading: true)
        
        transactionsWorker.loadTransactions { [weak self] result in
            guard let self = self else { return }
            
            self.presenter.presentLoading(isLoading: false)
            
            switch result {
            case let .success(transactions):
                let response = TransactionsModels.Load.Response(transactions: transactions.map {
                    TransactionsModels.Load.Response.Transaction(
                        id: $0.id,
                        amount: $0.amount,
                        vendor: $0.vendor,
                        category: $0.category,
                        date: $0.date
                    )
                })
                self.presenter.presentTransactions(response: response)
                
            case let .failure(error):
                let failureResponse = FailureResponse(errorMessage: error.localizedDescription)
                self.presenter.presentFailure(response: failureResponse)
            }
        }
    }
}
