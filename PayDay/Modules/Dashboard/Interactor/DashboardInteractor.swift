//
//  DashboardInteractor.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 05.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import Foundation

final class DashboardInteractor: DashboardInteractorInput {
    
    private let presenter: DashboardInteractorOutput
    
    private let transactionsWorker: TransactionsWorker
    
    private let calendarHelper = CalendarHelper()
    
    private lazy var currentMonth: Month = calendarHelper.month(from: Date())
    
    private var cache: [Month: [Transaction]] = [:]
    
    // MARK: - Init
    
    init(presenter: DashboardInteractorOutput, transactionsWorker: TransactionsWorker) {
        self.presenter = presenter
        self.transactionsWorker = transactionsWorker
    }
    
    // MARK: - Interactor Input
    
    func loadScreen(request: DashboardModels.Screen.Request) {
        let response = DashboardModels.Screen.Response()
        presenter.presentScreen(response: response)
    }
    
    func loadDashboard(request: DashboardModels.Load.Request) {
        let month: Month
        switch request.period {
        case .initial, .refresh:
            month = currentMonth
        case .next:
            month = calendarHelper.nextMonth(after: currentMonth)
        case .previous:
            month = calendarHelper.previousMonth(before: currentMonth)
        }
        currentMonth = month
        
        if let transactions = cache[month], request.period != .refresh {
            presentTransactions(transactions, month: month)
            return
        }
        loadTransactions(for: month)
    }
    
    private func loadTransactions(for month: Month) {
        presenter.presentLoading(isLoading: true)
        
        transactionsWorker.loadTransactions { [weak self] result in
            guard let self = self else { return }
            
            self.presenter.presentLoading(isLoading: false)
            
            switch result {
            case let .success(transactions):
                self.saveTransactions(transactions)
                self.presentTransactions(
                    transactions.filter { self.calendarHelper.month(from: $0.date) == month },
                    month: month
                )
            case let .failure(error):
                let failureResponse = FailureResponse(errorMessage: error.localizedDescription)
                self.presenter.presentFailure(response: failureResponse)
            }
        }
    }
    
    private func saveTransactions(_ transactions: [Transaction]) {
        cache.removeAll()
        
        for transaction in transactions {
            let month = calendarHelper.month(from: transaction.date)
            
            if cache[month] == nil {
                cache[month] = []
            }
            cache[month]?.append(transaction)
        }
    }
    
    private func presentTransactions(_ transactions: [Transaction], month: Month) {
        typealias Category = String
        typealias Amount = Decimal
        
        var data: [Category: Amount] = [:]
        for transaction in transactions {
            data[transaction.category] = (data[transaction.category] ?? 0) + transaction.amount
        }
        
        let categories = data
            .sorted { $0.value < $1.value }
            .map { DashboardModels.Load.Response.Category(name: $0.key, amount: $0.value) }
        
        let isNextPeriodAvailable = month != calendarHelper.month(from: Date())
        
        let response = DashboardModels.Load.Response(
            monthDate: calendarHelper.startDate(for: month),
            categories: categories,
            isPreviousPeriodAvailable: true,
            isNextPeriodAvailable: isNextPeriodAvailable
        )
        presenter.presentDashboard(response: response)
    }
}
