//
//  DashboardProtocols.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 05.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import PDUIKit

// MARK: - View

protocol DashboardViewInput: AnyObject, LoadingDisplayable, FailureDisplayable {
    func displayScreen(viewModel: DashboardModels.Screen.ViewModel)
    func displayDashboard(viewModel: DashboardModels.Load.ViewModel)
}

// MARK: - Interactor

protocol DashboardInteractorInput {
    func loadScreen(request: DashboardModels.Screen.Request)
    func loadDashboard(request: DashboardModels.Load.Request)
}

// MARK: - Presenter

protocol DashboardInteractorOutput: LoadingPresentable, FailurePresentable {
    func presentScreen(response: DashboardModels.Screen.Response)
    func presentDashboard(response: DashboardModels.Load.Response)
}

// MARK: - Router

protocol DashboardRouterInput: AlertPresentable {
}
