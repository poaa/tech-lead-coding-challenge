//
//  DashboardModels.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 05.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import Foundation

enum DashboardModels {
    
    enum Screen {
        struct Request {
        }
        struct Response {
        }
        struct ViewModel {
            let title: String
        }
    }
    
    enum Load {
        struct Request {
            enum Period {
                case initial
                case refresh
                case next
                case previous
            }
            let period: Period
        }
        struct Response {
            struct Category {
                let name: String
                let amount: Decimal
            }
            let monthDate: Date
            let categories: [Category]
            let isPreviousPeriodAvailable: Bool
            let isNextPeriodAvailable: Bool
        }
        struct ViewModel {
            enum State {
                case data([DashboardCategoryViewModel])
                case empty(placeholder: String)
            }
            let period: String
            let state: State
            let isPreviousPeriodAvailable: Bool
            let isNextPeriodAvailable: Bool
        }
    }
}
