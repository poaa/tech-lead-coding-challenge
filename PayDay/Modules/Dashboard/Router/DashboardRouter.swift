//
//  DashboardRouter.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 05.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import UIKit
import PDUIKit

final class DashboardRouter: DashboardRouterInput, AlertDisplayable {
    
    private(set) weak var viewController: UIViewController?
    
    init(viewController: UIViewController) {
        self.viewController = viewController
    }
}
