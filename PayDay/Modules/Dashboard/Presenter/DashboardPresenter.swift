//
//  DashboardPresenter.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 05.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import Foundation

final class DashboardPresenter: DashboardInteractorOutput {
    
    private weak var view: DashboardViewInput?
    
    private let formatter = DashboardDateFormatter()
    
    private let decimalFormatter = DecimalFormatter()
    
    // MARK: - Init
    
    init(view: DashboardViewInput) {
        self.view = view
    }
    
    // MARK: - Presentation
    
    func presentScreen(response: DashboardModels.Screen.Response) {
        let viewModel = DashboardModels.Screen.ViewModel(
            title: "My Dashboard"
        )
        view?.displayScreen(viewModel: viewModel)
    }
    
    func presentDashboard(response: DashboardModels.Load.Response) {
        let categories = response.categories.map {
            DashboardCategoryViewModel(
                categoryName: $0.name,
                amount: decimalFormatter.string(from: $0.amount)
            )
        }
        let state: DashboardModels.Load.ViewModel.State = categories.isEmpty
            ? .empty(placeholder: "You don't have transactions for this period")
            : .data(categories)
        
        let viewModel = DashboardModels.Load.ViewModel(
            period: formatter.periodString(from: response.monthDate),
            state: state,
            isPreviousPeriodAvailable: response.isPreviousPeriodAvailable,
            isNextPeriodAvailable: response.isNextPeriodAvailable
        )
        view?.displayDashboard(viewModel: viewModel)
    }
    
    func presentFailure(response: FailureResponse) {
        let viewModel = FailureViewModel(
            title: "Error",
            errorMessage: response.errorMessage
        )
        view?.displayFailure(viewModel: viewModel)
    }
    
    func presentLoading(isLoading: Bool) {
        view?.displayLoading(isLoading: isLoading)
    }
}
