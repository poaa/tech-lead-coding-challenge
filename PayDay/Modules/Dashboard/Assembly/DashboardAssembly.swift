//
//  DashboardAssembly.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 05.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import UIKit
import PDUIKit

struct DashboardAssembly {
    
    private let serviceFactory: ServiceFactory
    
    init(serviceFactory: ServiceFactory) {
        self.serviceFactory = serviceFactory
    }
    
    func build() -> UIViewController {
        let viewController = DashboardViewController.instantiateFromNib()
        let router = DashboardRouter(viewController: viewController)
        let presenter = DashboardPresenter(view: viewController)
        let interactor = DashboardInteractor(
            presenter: presenter,
            transactionsWorker: serviceFactory.makeTransactionsWorker()
        )
        viewController.set(interactor: interactor, router: router)
        
        return viewController
    }
}
