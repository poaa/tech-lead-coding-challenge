//
//  DashboardViewController.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 05.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import UIKit
import PDUIKit

final class DashboardViewController: UIViewController, NibInitializable, DashboardViewInput {
    
    private var interactor: DashboardInteractorInput?
    
    private var router: DashboardRouterInput?
    
    private var tableItems: [AnyCellViewModel] = []
    
    // MARK: - Subviews
    
    @IBOutlet private var periodTitleLabel: UILabel!
    
    @IBOutlet private var previousPeriodButton: UIButton!
    
    @IBOutlet private var nextPeriodButton: UIButton!
    
    @IBOutlet private var emptyStateLabel: UILabel!
    
    @IBOutlet private var tableView: UITableView!
    
    @IBOutlet private var activityIndicator: UIActivityIndicatorView!
    
    // MARK: - Injection
    
    func set(interactor: DashboardInteractorInput, router: DashboardRouterInput) {
        self.interactor = interactor
        self.router = router
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        loadScreen()
        loadData(period: .initial)
    }
    
    private func setupUI() {
        tableView.tableFooterView = UIView()
        tableView.register(DashboardCategoryViewModel.self)
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(actionRefreshEventTriggered(sender:)), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    private func loadScreen() {
        let request = DashboardModels.Screen.Request()
        interactor?.loadScreen(request: request)
    }
    
    private func loadData(period: DashboardModels.Load.Request.Period) {
        let request = DashboardModels.Load.Request(period: period)
        interactor?.loadDashboard(request: request)
    }
    
    // MARK: - Actions
    
    @objc private func actionRefreshEventTriggered(sender: UIRefreshControl) {
        loadData(period: .refresh)
    }
    
    @IBAction private func actionPreviousPeriodTapped(sender: UIButton) {
        loadData(period: .previous)
    }
    
    @IBAction private func actionNextPeriodTapped(sender: UIButton) {
        loadData(period: .next)
    }
    
    // MARK: - View Input
    
    func displayScreen(viewModel: DashboardModels.Screen.ViewModel) {
        navigationItem.title = viewModel.title
    }
    
    func displayDashboard(viewModel: DashboardModels.Load.ViewModel) {
        tableView.refreshControl?.endRefreshing()
        
        periodTitleLabel.text = viewModel.period
        previousPeriodButton.isEnabled = viewModel.isPreviousPeriodAvailable
        nextPeriodButton.isEnabled = viewModel.isNextPeriodAvailable
        
        switch viewModel.state {
        case let .data(categories):
            emptyStateLabel.text = nil
            tableItems = categories
            
        case let .empty(placeholder):
            emptyStateLabel.text = placeholder
            tableItems = []
        }
        tableView.reloadData()
    }
    
    func displayFailure(viewModel: FailureViewModel) {
        router?.showAlert(viewModel.title, message: viewModel.errorMessage, actionHandler: nil, completion: nil)
    }
    
    func displayLoading(isLoading: Bool) {
        [previousPeriodButton, nextPeriodButton].forEach {
            $0?.isEnabled = !isLoading
        }
        if isLoading {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
    }
}

// MARK: - Table View

extension DashboardViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(with: tableItems[indexPath.row], for: indexPath)
    }
}

extension DashboardViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
