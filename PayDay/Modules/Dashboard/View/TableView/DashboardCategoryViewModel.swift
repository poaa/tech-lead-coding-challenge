//
//  DashboardCategoryViewModel.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 05.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import PDUIKit

protocol DashboardCategoryViewModelData {
    var categoryName: String { get }
    var amount: String { get }
}

struct DashboardCategoryViewModel: CellViewModel, DashboardCategoryViewModelData {
    let categoryName: String
    let amount: String
    
    func setup(cell: DashboardCategoryTableViewCell) {
        cell.setup(data: self)
    }
}
