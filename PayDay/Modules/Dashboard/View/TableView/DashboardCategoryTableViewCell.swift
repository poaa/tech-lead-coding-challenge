//
//  DashboardCategoryTableViewCell.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 05.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import UIKit
import PDUIKit

final class DashboardCategoryTableViewCell: UITableViewCell, NibInitializable {
    
    @IBOutlet private var nameLabel: UILabel!
    
    @IBOutlet private var amountLabel: UILabel!
    
    func setup(data: DashboardCategoryViewModelData) {
        nameLabel.text = data.categoryName
        amountLabel.text = data.amount
    }
}
