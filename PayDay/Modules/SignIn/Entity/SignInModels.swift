//
//  SignInModels.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 04.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

enum SignInModels {
    
    enum Screen {
        struct Request {
        }
        struct Response {
            let email: String?
            let password: String?
        }
        struct ViewModel {
            let title: String
            let emailPlaceholder: String
            let passwordPlaceholder: String
            let buttonTitle: String
            let inputEmail: String?
            let inputPassword: String?
        }
    }
    
    enum SignIn {
        struct Request {
            let inputEmail: String?
            let inputPassword: String?
        }
        struct Response {
            let isEmailValid: Bool
            let isPasswordValid: Bool
        }
        struct ViewModel {
            let emailValidationErrorMessage: String?
            let passwordValidationErrorMessage: String?
        }
    }
    
    enum Success {
        struct Response {
        }
        struct ViewModel {
        }
    }
}
