//
//  SignInAssembly.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 04.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import UIKit
import PDUIKit

struct SignInAssembly {
    
    private let serviceFactory: ServiceFactory
    
    private weak var delegate: SignInRouterDelegate?
    
    init(serviceFactory: ServiceFactory, delegate: SignInRouterDelegate?) {
        self.serviceFactory = serviceFactory
        self.delegate = delegate
    }
    
    func build() -> UIViewController {
        let viewController = SignInViewController.instantiateFromNib()
        let router = SignInRouter(
            viewController: viewController,
            delegate: delegate
        )
        let presenter = SignInPresenter(view: viewController)
        let interactor = SignInInteractor(
            presenter: presenter,
            signInWorker: serviceFactory.makeSignInWorker()
        )
        viewController.set(interactor: interactor, router: router)
        
        return viewController
    }
}
