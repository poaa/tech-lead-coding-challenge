//
//  SignInViewController.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 04.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import UIKit
import PDUIKit

final class SignInViewController: UIViewController, NibInitializable, SignInViewInput, KeyboardHandler {
    
    private var interactor: SignInInteractorInput?
    
    private var router: SignInRouterInput?
    
    // MARK: - Subviews
    
    @IBOutlet private var scrollView: UIScrollView!

    @IBOutlet private var logoImageView: UIImageView!
    
    @IBOutlet private var emailTextField: UITextField!
    
    @IBOutlet private var emailErrorLabel: UILabel!
    
    @IBOutlet private var passwordTextField: UITextField!
    
    @IBOutlet private var passwordErrorLabel: UILabel!
    
    @IBOutlet private var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet private var actionButton: UIButton!
    
    var keyboardScrollView: UIScrollView {
        return scrollView
    }
    
    var keyboardInputViews: [KeyboardInputView] {
        return [emailTextField, passwordTextField]
    }
    
    // MARK: - Injection
    
    func set(interactor: SignInInteractorInput, router: SignInRouterInput) {
        self.interactor = interactor
        self.router = router
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let request = SignInModels.Screen.Request()
        interactor?.loadScreen(request: request)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        registerForKeyboardNotifications()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        unregisterForKeyboardNotifications()
    }
    
    // MARK: - Actions
    
    @IBAction private func actionSignInButtonTapped(sender: UIButton) {
        let email = emailTextField.text
        let password = passwordTextField.text
        
        let request = SignInModels.SignIn.Request(inputEmail: email, inputPassword: password)
        interactor?.performSignIn(request: request)
    }
    
    // MARK: - View Input
    
    func displayScreen(viewModel: SignInModels.Screen.ViewModel) {
        title = viewModel.title
        
        emailTextField.placeholder = viewModel.emailPlaceholder
        emailTextField.text = viewModel.inputEmail
        emailErrorLabel.text = nil
        
        passwordTextField.placeholder = viewModel.passwordPlaceholder
        passwordTextField.text = viewModel.inputPassword
        passwordErrorLabel.text = nil
        
        actionButton.setTitle(viewModel.buttonTitle, for: .normal)
    }
    
    func displayValidationResponse(viewModel: SignInModels.SignIn.ViewModel) {
        emailErrorLabel.text = viewModel.emailValidationErrorMessage
        passwordErrorLabel.text = viewModel.passwordValidationErrorMessage
    }

    func displaySuccess(viewModel: SignInModels.Success.ViewModel) {
        router?.finish()
    }
    
    func displayFailure(viewModel: FailureViewModel) {
        router?.showAlert(viewModel.title, message: viewModel.errorMessage, actionHandler: nil, completion: nil)
    }
    
    func displayLoading(isLoading: Bool) {
        actionButton.isEnabled = !isLoading
        
        if isLoading {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
    }
}

// MARK: - UITextFieldDelegate

extension SignInViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        moveToNextInput()
        return false
    }
}
