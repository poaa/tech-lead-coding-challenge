//
//  SignInRouter.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 04.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import UIKit
import PDUIKit

protocol SignInRouterDelegate: AnyObject {
    func signInDidFinish()
}

final class SignInRouter: SignInRouterInput, AlertDisplayable {
    
    private(set) weak var viewController: UIViewController?
    
    private weak var delegate: SignInRouterDelegate?
    
    init(viewController: UIViewController, delegate: SignInRouterDelegate?) {
        self.viewController = viewController
        self.delegate = delegate
    }
    
    func finish() {
        delegate?.signInDidFinish()
    }
}
