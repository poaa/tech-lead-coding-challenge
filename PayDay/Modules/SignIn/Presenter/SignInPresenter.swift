//
//  SignInPresenter.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 04.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

final class SignInPresenter: SignInInteractorOutput {
    
    private weak var view: SignInViewInput?
    
    // MARK: - Init
    
    init(view: SignInViewInput) {
        self.view = view
    }
    
    // MARK: - Presentation
    
    func presentScreen(response: SignInModels.Screen.Response) {
        let viewModel = SignInModels.Screen.ViewModel(
            title: "Sign In",
            emailPlaceholder: "Enter Email",
            passwordPlaceholder: "Enter Password",
            buttonTitle: "Sign In",
            inputEmail: response.email,
            inputPassword: response.password
        )
        view?.displayScreen(viewModel: viewModel)
    }
    
    func presentValidationResponse(response: SignInModels.SignIn.Response) {
        let emailMessage = response.isEmailValid ? nil : "Invalid Email"
        let passwordMessage = response.isPasswordValid ? nil : "Password must contain 6 or more alphanumeric characters"
        
        let viewModel = SignInModels.SignIn.ViewModel(
            emailValidationErrorMessage: emailMessage,
            passwordValidationErrorMessage: passwordMessage
        )
        view?.displayValidationResponse(viewModel: viewModel)
    }
    
    func presentSuccess(response: SignInModels.Success.Response) {
        let viewModel = SignInModels.Success.ViewModel()
        view?.displaySuccess(viewModel: viewModel)
    }
    
    func presentFailure(response: FailureResponse) {
        let viewModel = FailureViewModel(
            title: "Error",
            errorMessage: response.errorMessage
        )
        view?.displayFailure(viewModel: viewModel)
    }
    
    func presentLoading(isLoading: Bool) {
        view?.displayLoading(isLoading: isLoading)
    }
}
