//
//  SignInInteractor.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 04.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

final class SignInInteractor: SignInInteractorInput {
    
    private let presenter: SignInInteractorOutput
    
    private let signInWorker: SignInWorker
    
    private let emailValidator = EmailValidator()
    
    private let passwordValidator = PasswordValidator()
    
    // MARK: - Init
    
    init(presenter: SignInInteractorOutput, signInWorker: SignInWorker) {
        self.presenter = presenter
        self.signInWorker = signInWorker
    }
    
    // MARK: - Interactor Input
    
    func loadScreen(request: SignInModels.Screen.Request) {
        let response: SignInModels.Screen.Response
        #if DEBUG
        response = SignInModels.Screen.Response(email: DebugUser.email, password: DebugUser.password)
        #else
        response = SignInModels.Screen.Response(email: nil, password: nil)
        #endif
        
        presenter.presentScreen(response: response)
    }
    
    func performSignIn(request: SignInModels.SignIn.Request) {
        var isEmailValid = false
        if let email = request.inputEmail, emailValidator.isValid(inputValue: email) {
            isEmailValid = true
        }
        
        var isPasswordValid = false
        if let password = request.inputPassword, passwordValidator.isValid(inputValue: password) {
            isPasswordValid = true
        }
        
        let validationResponse = SignInModels.SignIn.Response(
            isEmailValid: isEmailValid,
            isPasswordValid: isPasswordValid
        )
        presenter.presentValidationResponse(response: validationResponse)
        
        guard let email = request.inputEmail, isEmailValid,
            let password = request.inputPassword, isPasswordValid else {
            return
        }
        
        signIn(email: email, password: password)
    }
    
    private func signIn(email: String, password: String) {
        presenter.presentLoading(isLoading: true)
        
        signInWorker.signIn(email: email, password: password) { [weak self] result in
            guard let self = self else { return }
            
            self.presenter.presentLoading(isLoading: false)
            
            switch result {
            case .success:
                let successResponse = SignInModels.Success.Response()
                self.presenter.presentSuccess(response: successResponse)
                
            case let .failure(error):
                let failureResponse = FailureResponse(errorMessage: error.localizedDescription)
                self.presenter.presentFailure(response: failureResponse)
            }
        }
    }
}
