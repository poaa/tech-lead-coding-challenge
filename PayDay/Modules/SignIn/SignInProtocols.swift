//
//  SignInProtocols.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 04.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import PDUIKit

// MARK: - View

protocol SignInViewInput: AnyObject, LoadingDisplayable, FailureDisplayable {
    func displayScreen(viewModel: SignInModels.Screen.ViewModel)
    func displayValidationResponse(viewModel: SignInModels.SignIn.ViewModel)
    func displaySuccess(viewModel: SignInModels.Success.ViewModel)
}

// MARK: - Interactor

protocol SignInInteractorInput {
    func loadScreen(request: SignInModels.Screen.Request)
    func performSignIn(request: SignInModels.SignIn.Request)
}

// MARK: - Presenter

protocol SignInInteractorOutput: LoadingPresentable, FailurePresentable {
    func presentScreen(response: SignInModels.Screen.Response)
    func presentValidationResponse(response: SignInModels.SignIn.Response)
    func presentSuccess(response: SignInModels.Success.Response)
}

// MARK: - Router

protocol SignInRouterInput: AlertPresentable {
    func finish()
}
