//
//  AppDelegate.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 04.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import UIKit

@UIApplicationMain
final class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    private var rootCoordinator: Coordinator?
    
    private let serviceFactory: ServiceFactory = ServiceFactoryImpl(configurationReader: ConfigurationReader())
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow()
        rootCoordinator = RootCoordinator(window: window, serviceFactory: serviceFactory)
        rootCoordinator?.start()
        return true
    }
}
