//
//  ServiceFactory.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 04.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import PDNetwork
import PDStorage

protocol ServiceFactory {
    func makeSignInWorker() -> SignInWorker
    func makeTransactionsWorker() -> TransactionsWorker
}

final class ServiceFactoryImpl: ServiceFactory {
    
    private let configurationReader: ConfigurationReader
    
    private lazy var configuration = configurationReader.parseConfiguration()
    
    private lazy var networkClient = makeNetworkClient()
    
    private lazy var sessionStorage = makeSessionStorage()
    
    init(configurationReader: ConfigurationReader) {
        self.configurationReader = configurationReader
    }
    
    private func makeNetworkClientFactory() -> NetworkClientFactory {
        return URLSessionNetworkClientFactory()
    }
    
    private func makeNetworkClient() -> NetworkClient {
        let configuration = NetworkClientConfiguration(baseURL: self.configuration.api.baseURL)
        let factory = makeNetworkClientFactory()
        return factory.makeNetworkClient(configuration: configuration)
    }
    
    private func makeKeychainStorage() -> KeychainStorage {
        return KeychainStorage(serviceName: configuration.storage.keychainService)
    }
    
    private func makeSessionStorage() -> SessionStorage & SessionProvider {
        return SessionStorageImpl(keychain: makeKeychainStorage())
    }
    
    func makeSignInWorker() -> SignInWorker {
        return SignInWorkerImpl(
            networkClient: networkClient,
            sessionStorage: sessionStorage
        )
    }
    
    func makeTransactionsWorker() -> TransactionsWorker {
        return TransactionsWorkerImpl(
            networkClient: networkClient,
            sessionProvider: sessionStorage
        )
    }
}
