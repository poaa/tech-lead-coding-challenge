//
//  Configuration.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 06.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import Foundation

struct Configuration: Decodable {
    struct API: Decodable {
        let baseURL: URL
        
        enum CodingKeys: String, CodingKey {
            case baseURL
        }
        
        init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            baseURL = URL(string: try container.decode(String.self, forKey: .baseURL))!
        }
    }
    struct Storage: Decodable {
        let keychainService: String
    }
    let api: API
    let storage: Storage
}
