//
//  Config.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 05.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import Foundation

final class ConfigurationReader {
    
    func parseConfiguration() -> Configuration {
        let decoder = PropertyListDecoder()
        
        return Bundle.main
            .url(forResource: "Configuration", withExtension: "plist")
            .flatMap { try? Data(contentsOf: $0) }
            .flatMap { try? decoder.decode(Configuration.self, from: $0) }!
    }
}
