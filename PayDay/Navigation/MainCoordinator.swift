//
//  MainCoordinator.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 05.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import UIKit

final class MainCoordinator: Coordinator {
    
    private let navigationController: UINavigationController
    
    private let serviceFactory: ServiceFactory
    
    // MARK: - Init
    
    init(navigationController: UINavigationController, serviceFactory: ServiceFactory) {
        self.navigationController = navigationController
        self.serviceFactory = serviceFactory
    }
    
    // MARK: - App Launch
    
    func start() {
        let viewControllers: [UIViewController] = [
            makeDashboardViewContainer(),
            makeTransactionsViewContainer()
        ]
        
        let tabBarViewController = UITabBarController()
        tabBarViewController.modalPresentationStyle = .fullScreen
        tabBarViewController.modalTransitionStyle = .crossDissolve
        tabBarViewController.setViewControllers(viewControllers, animated: false)
        
        navigationController.present(tabBarViewController, animated: true, completion: nil)
    }
    
    // MARK: - Dashboard
    
    private func makeDashboardViewContainer() -> UINavigationController {
        let assembly = DashboardAssembly(serviceFactory: serviceFactory)
        
        let viewController = assembly.build()
        viewController.tabBarItem = UITabBarItem(title: "Dashboard", image: UIImage(named: "dashboard"), tag: 0)
        
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.navigationBar.isTranslucent = false
        
        return navigationController
    }
    
    // MARK: - Transactions
    
    private func makeTransactionsViewContainer() -> UINavigationController {
        let assembly = TransactionsAssembly(serviceFactory: serviceFactory)
        
        let viewController = assembly.build()
        viewController.tabBarItem = UITabBarItem(title: "Transactions", image: UIImage(named: "transactionsList"), tag: 1)
        
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.navigationBar.isTranslucent = false
        
        return navigationController
    }
}
