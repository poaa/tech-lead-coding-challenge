//
//  AuthCoordinator.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 05.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import UIKit

protocol AuthCoordinatorDelegate: AnyObject {
    func didFinishAuthFlow()
}

final class AuthCoordinator: Coordinator {
    
    private let navigationController: UINavigationController
    
    private let serviceFactory: ServiceFactory
    
    private weak var delegate: AuthCoordinatorDelegate?
    
    // MARK: - Init
    
    init(navigationController: UINavigationController, serviceFactory: ServiceFactory, delegate: AuthCoordinatorDelegate?) {
        self.navigationController = navigationController
        self.serviceFactory = serviceFactory
        self.delegate = delegate
    }
    
    // MARK: - App Launch
    
    func start() {
        let signInViewController = makeSignInViewController()
        navigationController.pushViewController(signInViewController, animated: false)
    }
    
    private func makeSignInViewController() -> UIViewController {
        let assembly = SignInAssembly(serviceFactory: serviceFactory, delegate: self)
        return assembly.build()
    }
}

// MARK: - Sign In Delegate

extension AuthCoordinator: SignInRouterDelegate {
    
    func signInDidFinish() {
        delegate?.didFinishAuthFlow()
    }
}
