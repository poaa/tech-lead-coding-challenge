//
//  RootCoordinator.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 04.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import UIKit

final class RootCoordinator: Coordinator {
    
    private weak var window: UIWindow?
    
    private let rootViewController: UINavigationController
    
    private let serviceFactory: ServiceFactory
    
    private var authCoordinator: Coordinator?
    
    private var mainCoordinator: Coordinator?
    
    // MARK: - Init
    
    init(window: UIWindow?, serviceFactory: ServiceFactory) {
        self.window = window
        self.rootViewController = UINavigationController()
        self.serviceFactory = serviceFactory
        self.authCoordinator = AuthCoordinator(
            navigationController: rootViewController,
            serviceFactory: serviceFactory,
            delegate: self
        )
        self.mainCoordinator = MainCoordinator(
            navigationController: rootViewController,
            serviceFactory: serviceFactory
        )
    }
    
    // MARK: - App Launch
    
    func start() {
        rootViewController.navigationBar.isTranslucent = false
        window?.rootViewController = rootViewController
        
        // Root coordinator usually is responsible for performing app initialization like:
        // - showing splash/loading screen while initial data is pre-loading from the server
        // - handling deep links
        // But in our project we still don't have such requirements, so just delegate flow to Auth coordinator
        authCoordinator?.start()
        
        window?.makeKeyAndVisible()
    }
}

// MARK: - Auth Delegate

extension RootCoordinator: AuthCoordinatorDelegate {
    
    func didFinishAuthFlow() {
        mainCoordinator?.start()
    }
}
