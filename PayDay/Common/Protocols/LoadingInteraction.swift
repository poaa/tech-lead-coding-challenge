//
//  LoadingInteractive.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 06.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import Foundation

protocol LoadingPresentable {
    func presentLoading(isLoading: Bool)
}

protocol LoadingDisplayable {
    func displayLoading(isLoading: Bool)
}
