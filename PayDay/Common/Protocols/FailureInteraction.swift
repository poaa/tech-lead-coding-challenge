//
//  FailureInteraction.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 06.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

struct FailureResponse {
    let errorMessage: String
}

protocol FailurePresentable {
    func presentFailure(response: FailureResponse)
}

struct FailureViewModel {
    let title: String
    let errorMessage: String
}

protocol FailureDisplayable {
    func displayFailure(viewModel: FailureViewModel)
}
