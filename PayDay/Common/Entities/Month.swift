//
//  Month.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 05.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

struct Month: Hashable {
    let month: Int
    let year: Int
}
