//
//  User.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 04.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import Foundation

struct User {
    let id: Int
    let firstName: String
    let lastName: String
    let gender: Gender
    let email: String
    let password: String
    let dateOfBirth: Date
    let phone: String
    let accounts: [Account]
    
    enum Gender: String {
        case male
        case female
    }
    
    struct Account {
        let id: Int
        let customerId: Int
        let active: Bool
        let type: String
        let iban: String
        let createdDate: Date
    }
}
