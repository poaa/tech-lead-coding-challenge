//
//  Transaction.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 05.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import Foundation

struct Transaction {
    let id: Int
    let accountId: Int
    let amount: Decimal
    let vendor: String
    let category: String
    let date: Date
}
