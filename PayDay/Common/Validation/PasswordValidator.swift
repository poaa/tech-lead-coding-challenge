//
//  PasswordValidator.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 04.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import Foundation

struct PasswordValidator: Validator {
    
    func isValid(inputValue: String) -> Bool {
        guard inputValue.count >= 6 else {
            return false
        }
        return inputValue.rangeOfCharacter(from: CharacterSet.alphanumerics.inverted) == nil
    }
}
