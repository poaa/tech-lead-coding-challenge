//
//  Validator.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 04.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

protocol Validator {
    associatedtype InputValue
    func isValid(inputValue: InputValue) -> Bool
}
