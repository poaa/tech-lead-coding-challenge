//
//  TransactionDateFormatter.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 05.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import Foundation

struct TransactionDateFormatter {
    
    private let currentYearFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d, h:mm a"
        return formatter
    }()
    
    private let oldYearFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d yyyy, h:mm a"
        return formatter
    }()
    
    func string(from date: Date) -> String {
        return date.isCurrentYear
            ? currentYearFormatter.string(from: date)
            : oldYearFormatter.string(from: date)
    }
}
