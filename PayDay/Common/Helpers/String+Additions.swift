//
//  String+Additions.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 06.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import Foundation

extension String {
    
    func date(format: DateFormat) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format.rawValue
        dateFormatter.timeZone = TimeZone(identifier: "GMT")
        return dateFormatter.date(from: self)
    }
}
