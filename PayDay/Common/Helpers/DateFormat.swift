//
//  DateFormat.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 04.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import Foundation

/// Date format from API.
enum DateFormat: String {
    case common     = "yyyy-MM-dd'T'HH:mm:ss'Z'"
    case customer   = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    case account    = "MM/dd/yyyy"
}
