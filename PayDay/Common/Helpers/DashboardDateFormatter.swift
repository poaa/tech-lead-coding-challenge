//
//  DashboardDateFormatter.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 06.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import Foundation

struct DashboardDateFormatter {
    
    private let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM yyyy"
        return formatter
    }()
    
    func periodString(from date: Date) -> String {
        return formatter.string(from: date)
    }
}
