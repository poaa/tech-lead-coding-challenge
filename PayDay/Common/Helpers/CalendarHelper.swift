//
//  CalendarHelper.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 05.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import Foundation

struct CalendarHelper {
    
    func month(from date: Date) -> Month {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month], from: date)
        return Month(month: components.month!, year: components.year!)
    }
    
    func nextMonth(after month: Month) -> Month {
        guard let nextMonthStartDate = Calendar.current.date(byAdding: .month, value: 1, to: startDate(for: month)) else {
            // should never happen
            fatalError("nextMonthStartDate = nil")
        }
        return self.month(from: nextMonthStartDate)
    }
    
    func previousMonth(before month: Month) -> Month {
        guard let previousMonthStartDate = Calendar.current.date(byAdding: .month, value: -1, to: startDate(for: month)) else {
            // should never happen
            fatalError("previousMonthStartDate = nil")
        }
        return self.month(from: previousMonthStartDate)
    }
    
    func startDate(for month: Month) -> Date {
        let calendar = Calendar.current
        let components = DateComponents(calendar: calendar, year: month.year, month: month.month, day: 1)
        return calendar.date(from: components) ?? Date()
    }
    
    func endDate(for month: Month) -> Date {
        return startDate(for: nextMonth(after: month)).addingTimeInterval(-1)
    }
}
