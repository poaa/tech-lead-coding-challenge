//
//  DecimalFormatter.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 05.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import Foundation

struct DecimalFormatter {
    
    private let numberFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 2
        
        let groupingFormat = "###,###,###,###.##"
        formatter.positiveFormat = "\(groupingFormat)"
        formatter.negativeFormat = "-\(groupingFormat)"
        
        let groupSeparator = ","
        formatter.groupingSeparator = groupSeparator
        formatter.currencyGroupingSeparator = groupSeparator
        
        return formatter
    }()
    
    func string(from value: Decimal) -> String {
        return numberFormatter.string(for: value) ?? ""
    }
}
