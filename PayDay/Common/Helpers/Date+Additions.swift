//
//  Date+Additions.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 06.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import Foundation

extension Date {
    
    func string(format: DateFormat) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format.rawValue
        dateFormatter.timeZone = TimeZone(identifier: "GMT")
        return dateFormatter.string(from: self)
    }
    
    var isCurrentYear: Bool {
        let calendar = Calendar.current
        guard let year = calendar.dateComponents([.year], from: self).year,
            let currentYear = calendar.dateComponents([.year], from: Date()).year else {
                return false
        }
        return year == currentYear
    }
}
