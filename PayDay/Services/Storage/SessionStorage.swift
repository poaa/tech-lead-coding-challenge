//
//  SessionStorage.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 04.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import Foundation
import PDStorage

protocol SessionStorage {
    func save(user: User) throws
}

protocol SessionProvider {
    var accounts: [User.Account]? { get }
}

final class SessionStorageImpl: SessionStorage, SessionProvider {
    
    private enum Keys {
        static let customerId = "customerId"
        static let firstName = "firstName"
        static let lastName = "lastName"
        static let gender = "gender"
        static let email = "email"
        static let dateOfBirth = "dateOfBirth"
        static let phone = "phone"
    }
    
    private let keychain: KeychainStorage
    
    private(set) var accounts: [User.Account]?
    
    init(keychain: KeychainStorage) {
        self.keychain = keychain
    }
    
    func save(user: User) throws {
        try keychain.setValue(user.id, forKey: Keys.customerId)
        try keychain.setValue(user.firstName, forKey: Keys.firstName)
        try keychain.setValue(user.lastName, forKey: Keys.lastName)
        try keychain.setValue(user.gender.rawValue, forKey: Keys.gender)
        try keychain.setValue(user.email, forKey: Keys.email)
        try keychain.setValue(user.dateOfBirth.string(format: .customer), forKey: Keys.dateOfBirth)
        try keychain.setValue(user.phone, forKey: Keys.phone)
        
        // Accounts can be stored inside database, but to simplify the test task we will store them in memory.
        self.accounts = user.accounts
    }
}
