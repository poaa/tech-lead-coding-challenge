//
//  SignInWorker.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 04.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import PDNetwork

protocol SignInWorker {
    func signIn(email: String, password: String, completion: @escaping (Result<Void, Error>) -> Void)
}

final class SignInWorkerImpl: SignInWorker {
    
    private let networkClient: NetworkClient
    
    private let sessionStorage: SessionStorage
    
    init(networkClient: NetworkClient, sessionStorage: SessionStorage) {
        self.networkClient = networkClient
        self.sessionStorage = sessionStorage
    }
    
    func signIn(email: String, password: String, completion: @escaping (Result<Void, Error>) -> Void) {
        sendSignIn(email: email, password: password) { result in
            switch result {
            case let .success(customerResponse):
                self.getAccounts(customerId: customerResponse.id) { accountResult in
                    switch accountResult {
                    case let .success(accountsResponse):
                        do {
                            let user = try self.makeUser(
                                customer: customerResponse,
                                accounts: accountsResponse
                            )
                            try self.sessionStorage.save(user: user)
                            completion(.success(()))
                            
                        } catch {
                            completion(.failure(error))
                        }
                    case let.failure(error):
                        completion(.failure(error))
                    }
                }
            case let .failure(error):
                completion(.failure(error))
            }
        }
    }
    
    private func sendSignIn(email: String, password: String, completion: @escaping (Result<SignInResponse, Error>) -> Void) {
        let request = SignInRequest(email: email, password: password)
        networkClient.perform(request, completion: completion)
    }
    
    private func getAccounts(customerId: Int, completion: @escaping (Result<[AccountResponse.Account], Error>) -> Void) {
        let request = AccountRequest(customerId: customerId)
        networkClient.perform(request, completion: completion)
    }
    
    private func makeUser(customer: SignInResponse, accounts: [AccountResponse.Account]) throws -> User {
        guard !accounts.isEmpty else {
            // To simplify the test task we will require that user have at least one account which will be used later.
            throw SignInWorkerError.emptyAccountsList
        }
        guard let dateOfBirth = customer.dateOfBirth.date(format: .customer) else {
            throw SignInWorkerError.invalidDateFormat
        }
        
        let gender: User.Gender
        switch customer.gender {
        case .male:
            gender = .male
        case .female:
            gender = .female
        }
        return User(
            id: customer.id,
            firstName: customer.firstName,
            lastName: customer.lastName,
            gender: gender,
            email: customer.email,
            password: customer.password,
            dateOfBirth: dateOfBirth,
            phone: customer.phone,
            accounts: accounts.compactMap {
                guard let createdDate = $0.createdDate.date(format: .account) else {
                    return nil
                }
                return User.Account(
                    id: $0.id,
                    customerId: customer.id,
                    active: $0.active,
                    type: $0.type,
                    iban: $0.iban,
                    createdDate: createdDate
                )
            }
        )
    }
}
