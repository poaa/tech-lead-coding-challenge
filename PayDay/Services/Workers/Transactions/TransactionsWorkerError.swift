//
//  TransactionsWorkerError.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 05.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

enum TransactionsWorkerError: Error {
    case emptyAccountId
    case invalidDateFormat
    case invalidDecimalFormat
}
