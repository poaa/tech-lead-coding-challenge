//
//  TransactionsWorker.swift
//  PayDay
//
//  Created by Anton Poltoratskyi on 05.07.2020.
//  Copyright © 2020 Andersen. All rights reserved.
//

import Foundation
import PDNetwork

protocol TransactionsWorker {
    func loadTransactions(completion: @escaping (Result<[Transaction], Error>) -> Void)
}

final class TransactionsWorkerImpl: TransactionsWorker {
    
    private let networkClient: NetworkClient
    
    private let sessionProvider: SessionProvider
    
    init(networkClient: NetworkClient, sessionProvider: SessionProvider) {
        self.networkClient = networkClient
        self.sessionProvider = sessionProvider
    }
    
    func loadTransactions(completion: @escaping (Result<[Transaction], Error>) -> Void) {
        // To simplify the test task we will use only first active account.
        guard let accountId = sessionProvider.accounts?.first?.id else {
            completion(.failure(TransactionsWorkerError.emptyAccountId))
            return
        }
        let request = TransactionsRequest(accountId: accountId)
        networkClient.perform(request) { (result: Result<[TransactionsResponse.Transaction], Error>) in
            completion(result.map { transactions -> [Transaction] in
                return transactions
                    .compactMap { transaction -> Transaction? in
                        try? self.makeTransaction(from: transaction)
                    }
                    .sorted { $0.date > $1.date }
            })
        }
    }
    
    private func makeTransaction(from response: TransactionsResponse.Transaction) throws -> Transaction {
        guard let date = response.date.date(format: .common) else {
            throw TransactionsWorkerError.invalidDateFormat
        }
        guard let amount = Decimal(string: response.amount) else {
            throw TransactionsWorkerError.invalidDecimalFormat
        }
        return Transaction(
            id: response.id,
            accountId: response.accountId,
            amount: amount,
            vendor: response.vendor,
            category: response.category,
            date: date
        )
    }
}
